/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "i2c.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "tsl2561.h"
#include "lcd.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_USART2_UART_Init();
  MX_ADC2_Init();
  /* USER CODE BEGIN 2 */

	HAL_GPIO_WritePin(PWR_ON_GPIO_Port, PWR_ON_Pin, GPIO_PIN_SET);

	HAL_ADC_Start(&hadc2);
	
	Adafruit_LCD_begin();
	Adafruit_LCD_display();
	Adafruit_LCD_clearDisplay();
	Adafruit_LCD_setCursor(0,0);
	Adafruit_LCD_print("Luxb2ad", 7);
	Adafruit_LCD_display();
	
	bool isConnected = TSL2561_begin();

	const uint8_t time_capture[25] = {0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180};
	double lux_capture[25];
	uint32_t capture_index = 0;
		
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	unsigned long startTime = HAL_GetTick();
  while (1)
  {
		uint32_t lux = NULL;
		unsigned long currentTime = HAL_GetTick()- startTime;

		
		if(isConnected) {
			TSL2561_enable();
			lux = TSL2561_getLux();
			TSL2561_disable();
			
			uint32_t battery = 0;
		
			HAL_ADC_Start(&hadc2);
			if(HAL_ADC_PollForConversion(&hadc2, 1000) == HAL_OK)
			{
				battery = HAL_ADC_GetValue(&hadc2);
			}
			HAL_ADC_Stop(&hadc2);
		
			
			if (lux == NULL)
				lux = 0;
			
			if(time_capture[capture_index] <= currentTime/1000)
			{
				lux_capture[capture_index++] = lux;
			}
			
			char buffer[60];
			sprintf(buffer, "Time: %ld, Lux: %d, Battery: %4.2fV\n", currentTime, lux, (double)battery/4096 * 3.3);
			//*buffer = currentTime;
			//*(buffer+4) = lux;
			HAL_UART_Transmit(&huart2, (uint8_t *) buffer,60, HAL_MAX_DELAY);
				
			Adafruit_LCD_clearDisplay();
			Adafruit_LCD_setCursor(0, 0);
			Adafruit_LCD_print("t(s) :", 6);
			Adafruit_LCD_printInt(currentTime/1000, 10);
			Adafruit_LCD_print("\n", 1);
			Adafruit_LCD_print("I(lx):", 6);
			Adafruit_LCD_printFloat(lux,1);
			//Adafruit_LCD_printInt(battery, 10);
			
			//Vertical Lines
			Adafruit_LCD_fillRect(0,LCDHEIGHT-27,1,27, BLACK);
			Adafruit_LCD_fillRect(LCDWIDTH-18, LCDHEIGHT-27,1,27, BLACK);

			//Horizontal Lines
			Adafruit_LCD_fillRect(0,LCDHEIGHT-27,LCDWIDTH-18,1, BLACK);
			Adafruit_LCD_fillRect(0,LCDHEIGHT-1,LCDWIDTH-18,1, BLACK);
			
			if(battery < 900)
			{
				Adafruit_LCD_fillRect(LCDWIDTH-13,LCDHEIGHT-16,12,15, BLACK);
				Adafruit_LCD_fillRect(LCDWIDTH-10, LCDHEIGHT-18, 6, 2, BLACK);
				Adafruit_LCD_fillRect(LCDWIDTH-12, LCDHEIGHT-15, 10, 13, WHITE);
				Adafruit_LCD_fillRect(LCDWIDTH-11, LCDHEIGHT-5, 8, 2, BLACK);
			}
			
			Adafruit_LCD_setCursor(4,23);
			Adafruit_LCD_print("t(s)", 4);
			Adafruit_LCD_setCursor(LCDWIDTH/2-7, 23);
			Adafruit_LCD_print("I(lx)", 5);
			
			if(capture_index >= 1)
			{
				Adafruit_LCD_setCursor(4,31);
				Adafruit_LCD_printInt(time_capture[capture_index-1], 10);
				Adafruit_LCD_setCursor(LCDWIDTH/2-7, 31);
				Adafruit_LCD_printFloat(lux_capture[capture_index-1],1);
			}
			if(capture_index > 1)
			{
				Adafruit_LCD_setCursor(4,39);
				Adafruit_LCD_printInt(time_capture[capture_index-2], 10);
				Adafruit_LCD_setCursor(LCDWIDTH/2-7, 39);
				Adafruit_LCD_printFloat(lux_capture[capture_index-2],1);
			}
			
			Adafruit_LCD_display();
		}
		else
		{
			Adafruit_LCD_clearDisplay();
      Adafruit_LCD_setCursor(0, 0);
      Adafruit_LCD_print("Connect Lux\nSensor and\nrestart", 30);
			Adafruit_LCD_display();
		}
			
		
		
		if(HAL_GPIO_ReadPin(BTN_GRAY_GPIO_Port, BTN_GRAY_Pin) == GPIO_PIN_RESET || currentTime > 200000)
			HAL_GPIO_WritePin(PWR_ON_GPIO_Port, PWR_ON_Pin, GPIO_PIN_RESET);
		
		if(HAL_GPIO_ReadPin(BTN_YELLOW_GPIO_Port, BTN_YELLOW_Pin) == GPIO_PIN_RESET)
		{
			capture_index = 0;
			startTime = HAL_GetTick();
		}
		
	}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
